#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2023-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse
from configobj import ConfigObj, ConfigObjError
import fnmatch
from io import StringIO
import itertools
import logging
import os
import re
import requests
import sys

from jinja2 import (
    Environment,
    FileSystemLoader,
    StrictUndefined,
    make_logging_undefined,
)
from jinja2.exceptions import UndefinedError, TemplateSyntaxError
import yaml
from generate_plans import __version__

import yaml

FullLoader = None
for loader in ["CFullLoader", "CLoader", "FullLoader", "Loader"]:
    if hasattr(yaml, loader):
        if loader != "CFullLoader":
            print("Warning: using python yaml loader")
        FullLoader = getattr(yaml, loader)
        break

assert FullLoader is not None


def yaml_load(data):
    return yaml.load(data, Loader=FullLoader)

FORMAT = "[%(funcName)16s() ] %(message)s"
logging.basicConfig(level=logging.INFO, format=FORMAT)
logger = logging.getLogger(__name__)


try:
    from urllib.parse import urlsplit
except ImportError:
    from urlparse import urlsplit


# Templates base path
script_dirname = os.path.dirname(os.path.abspath(__file__))
template_include_base_path = os.path.join(script_dirname, "templates/include")
template_plan_base_path = os.path.join(script_dirname, "templates/plan")



def _load_template(template_name, template_path, toolchain):
    template = ""
    template_file_name = ""

    if template_name:
        template_file_name = "%s/%s/%s" % (template_path, toolchain, template_name)
        if os.path.exists(template_file_name):
            with open(template_file_name, "r") as f:
                template = f.read()
        else:
            logger.error(
                "template (%s) was specified but does not exist" % template_file_name
            )
            return 1

    return template, template_file_name


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--version", action="version", version=f"%(prog)s, {__version__}"
    )

    parser.add_argument(
        "--toolchain", help="toolchain", dest="toolchain", required=True
    )
    parser.add_argument(
        "--plan-name",
        help="Template plan name",
        required=True,
    )
    parser.add_argument(
        "--template-plan-path",
        help="Path to plan templates",
        dest="template_plan_base_path",
        default=template_plan_base_path,
    )
    parser.add_argument(
        "--template-include-path",
        help="Path to include templates",
        dest="template_include_base_path",
        default=template_include_base_path,
    )
    parser.add_argument(
        "--verbose",
        help="""Verbosity level. Follows logging levels:
                          CRITICAL: 50
                          ERROR: 40
                          WARNING: 30
                          INFO: 20
                          DEBUG: 10
                          NOTSET: 0""",
        dest="verbose",
        type=int,
        default=logging.INFO,
    )

    args = parser.parse_args()
    logger.setLevel(args.verbose)
    exit_code = 0

    output_path = os.path.abspath(os.path.join(script_dirname, "..", "tmp"))


    if not os.path.exists(output_path):
        os.makedirs(output_path, exist_ok=True)

    template_dirs = [
        os.path.abspath(template_plan_base_path),
        os.path.abspath(template_include_base_path),
    ]
    # prevent creating templates when variables are missing
    j2_env = Environment(
        loader=FileSystemLoader(template_dirs, followlinks=True),
        undefined=make_logging_undefined(logger=logger, base=StrictUndefined)
    )
    context = {}
    context.update({"toolchain": args.toolchain})
    jobs = []
    plan_file = []
    job = j2_env.get_template(args.plan_name).render(context)
    jobs.append(job)
    lines = ""
    for line in jobs:
        lines += line

    file = f'{output_path}/{args.plan_name.replace(".yaml.jinja2", "-"+args.toolchain+".yaml")}'
    with open(file, 'w') as fp:
        fp.write(lines)



if __name__ == "__main__":
    sys.exit(main())
