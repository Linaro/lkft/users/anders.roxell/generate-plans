#!/usr/bin/env python3
# Copyright 2023-present Linaro Limited
#
# SPDX-License-Identifier: MIT

from generate_plans.__main__ import main

main()
